package com.hans.wfcommon;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@ComponentScan({"com.hans.wfcommon"})
// @AutoConfigurationPackage  // for JPA
public class ModuleConfiguration {
    @PostConstruct
    public void init() {
        log.info("common init!");
    }
}
