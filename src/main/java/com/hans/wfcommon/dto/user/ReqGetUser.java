package com.hans.wfcommon.dto.user;

import lombok.Data;

@Data
public class ReqGetUser {
    Long uid;
}
