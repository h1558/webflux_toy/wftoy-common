package com.hans.wfcommon.dto.user;

import lombok.Data;

@Data
public class ReqRemoveUser {
    Long uid;
}
