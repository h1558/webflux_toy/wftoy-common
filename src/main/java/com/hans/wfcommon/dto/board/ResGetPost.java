package com.hans.wfcommon.dto.board;

import com.hans.wfcommon.entity.CommentWithName;
import com.hans.wfcommon.entity.PostWithName;
import lombok.AllArgsConstructor;
import lombok.Data;
import reactor.util.function.Tuple2;

import java.util.List;

@Data
@AllArgsConstructor
public class ResGetPost {
    private List<CommentWithName> pagedComments;
    private PostWithName post;

    public ResGetPost(Tuple2<List<CommentWithName>, PostWithName> tuple2) {
        this.pagedComments = tuple2.getT1();
        this.post = tuple2.getT2();
    }
}
