package com.hans.wfcommon.dto.board;

import com.hans.wfcommon.entity.PostWithName;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ResGetPagedPosts {
    private List<PostWithName> posts;
    private String keyword;
}
