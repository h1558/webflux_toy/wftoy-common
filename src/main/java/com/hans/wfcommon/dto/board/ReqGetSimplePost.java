package com.hans.wfcommon.dto.board;

import lombok.Data;

@Data
public class ReqGetSimplePost {
    Long postId;
}
