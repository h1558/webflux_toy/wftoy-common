package com.hans.wfcommon.dto.board;

import lombok.Data;

@Data
public class ReqDeletePost {
    private Long postId;
    private Long uid;
}
