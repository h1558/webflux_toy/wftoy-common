package com.hans.wfcommon.dto.board;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResDeletePost {
    Long deletedPostId;
    Integer queryResult;
}
