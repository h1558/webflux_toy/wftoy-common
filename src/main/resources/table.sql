create table comments (
    comment_id  bigserial not null,
    contents varchar(255),
    created_at timestamp,
    deleted_at timestamp,
    is_deleted boolean,
    post_id int8,
    uid int8,
    updated_at timestamp,
    name varchar(255),
    primary key (comment_id));

create table posts (
    post_id  bigserial not null,
    contents varchar(255),
    created_at timestamp,
    delete_at timestamp,
    is_deleted boolean,
    title varchar(255),
    uid int8,
    updated_at timestamp,
    name varchar(255),
    primary key (post_id));

create table users (
    uid  bigserial not null,
    userid varchar(255),
    name varchar(255),
    pwd varchar(255),
    valid boolean,
    primary key (uid));