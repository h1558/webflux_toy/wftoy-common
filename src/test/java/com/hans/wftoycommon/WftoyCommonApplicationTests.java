package com.hans.wftoycommon;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WftoyCommonApplicationTests {

    @Test
    void contextLoads() {
    }

    @SpringBootApplication
    static class TestConfiguration {

    }

}
